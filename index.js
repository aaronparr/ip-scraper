#!/usr/bin/env node
const path = require('node:path');
const FS = require('node:fs');
const process = require('node:process');
const spawn = require('node:child_process').spawn;
const EL = require('os').EOL;
const C = require('./src/_colors.js').Colors;
const { log } = require('./src/_utils.js');

// GLOBAL
let thisIP = '0.0.0.0';
let gaveExitMessage = false;
let detailedScrape = true;
// let file_path = '';
// let file_name = 'scraped_ip_list';
// let accumulated_iplist = [];
// let wroteFile = false;

/**
 * Output the current IP we are lookingup while exiting
 * @param {*} exitCode 
 */
const handleProgramTermination = (exitCode) =>
{
  if(exitCode==='suppress')
    gaveExitMessage = true;
  if (!gaveExitMessage)
  {
    // log('Exited at '+ C.Green+thisIP+C.Reset, 'handleProgramTermination', exitCode, 'end', true);
    process.stdout.write(']\n');
    gaveExitMessage = true;
  }

  // if ( accumulated_iplist.length && !wroteFile )
  // {
  //   if (file_path!=='')
  //     file_path = path.join(process.cwd(), file_name);
  //   if (!file_path.includes('.json')){ file_path += '.json'; }
    
  //   if (FS.existsSync(file_path))
  //   {
  //     const JSONString = FS.readFileSync(file_path, 'utf8');
  //     accumulated_iplist = mergeIPLists(JSON.parse(JSONString), accumulated_iplist);
  //   }

  //   let iplist_file_content = JSON.stringify(accumulated_iplist, null, 2);
  //   try
  //   {
  //     FS.writeFileSync(file_path, iplist_file_content);
  //     wroteFile = true;
  //   }
  //   catch (err) { console.error(err); }
  // }

  process.exit();
}

// so the program will not close instantly
process.stdin.resume(); 
// do something when app is closing
process.on('exit', handleProgramTermination.bind(null));
// catches ctrl+c event
process.on('SIGINT', handleProgramTermination.bind(null));
// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', handleProgramTermination.bind(null));
process.on('SIGUSR2', handleProgramTermination.bind(null));
// catches uncaught exceptions
process.on('uncaughtException', handleProgramTermination.bind(null));


// --------------------------------------------------------------------
// CLI arguments and their options
const COMMANDS =
[
  'help','-h','--help',
  'ipaddress','-i','--ipaddress',
  'organization','-o','--organization',
  'location','-l','--location',
  '-1','-2','-3'
];
const OPTS =
{
};


// --------------------------------------------------------------------
// Functions

/**
 * Display Help
 * triggered by the arguments -h | help
 */
const helpFeedback = () =>
{
  console.log(
    EL+C.Reverse+' IP SCRAPER '+C.Reset+' : '+C.Bright+'Help'+C.Reset+EL
   +EL
   +'Scrapes data from the WHOIS tool about IP addresses.'+EL
   +''+EL
   +C.Bright+'USAGE:'+C.Reset+EL
   +C.Underscore+'ARG'+C.Reset+'                  '+C.Underscore+'MEANING'+C.Reset+EL
   +'[-1|-2|-3]             Specify the part of the IP address to increment: 1st, 2nd, or 3rd. Default is the 4th.'+EL
   +'[-i|'+C.Bright+'ipaddress'+C.Reset+']       Specify IP address to start scraping from.'+EL
   +'[-l|'+C.Bright+'location'+C.Reset+']        Specify string included in location address.'+EL
   +'[-o|'+C.Bright+'organization'+C.Reset+']    Specify string included in organization name.'+EL
   +'[-v|'+C.Bright+'verbose'+C.Reset+']         Print all to console.'+EL
   +''+EL
 );
};

/**
 * 
 * @param {*} address 
 * @param {*} recursion 
 * @returns 
 */
const lookup = async (address) =>
{
  return new Promise( (resolve, reject) =>
  {
    let persistantData = '';
    const whois = spawn('whois',[address]);

    whois.stdout.on('data', (data) => { persistantData += data.toString(); });
    
    // whois.stderr.on('data', (err) =>
    // {
    //   if (!err.includes('whois: connect(): Operation timed out'))
    //   {
    //     log(err, 'ipScraper', address, 'ERR', true); 
    //     reject(err);
    //   }
    //   // else
    //   // {
    //   //   const parsedData = parse(persistantData);
    //   //   resolve(parsedData);
    //   // }
    // });
    
    whois.on('close', () => { resolve(parse(persistantData, address)); });
  });
};

const ipToInt = (IP_addr) =>
{
  let parts = IP_addr.split('.').map(function(str){ return parseInt(str); });
  return ((parts[0] ? parts[0] * Math.pow(256, 3) : 0) + (parts[1] ? parts[1] * Math.pow(256, 2) : 0) + (parts[2] ? parts[2] * Math.pow(256, 1) : 0) + parts[3]);
};

class ParsedBlock {
  constructor()
  {
    this.ip =
    {
      'range': '',
      'CIDR': '',
      'network': ''
    },
    this.organization =
    {
      'name': '',
      'id':'',
      'description': ''
    },
    this.location =
    {
      'address': '',
      'city': '',
      'state': '',
      'postalcode': '',
      'country': ''
    },
    this.source =
    {
      'name': '',
      'status': '',
      'refer': ''
    }
  }
}

/**
 * 
 * @param {string} whoIsData 
 */
const parse = (whoIsData, parsedIPaddress) =>
{
  // each time we find an IP range we create a new block and add it to the list
  // we sort them at the end to determine which is the most appropriate to return
  // we have to do this because whois returns broader information than the current entity using the IP address.
  let parsedBlocks = [];
  let parsedBlockData = new ParsedBlock();
  let text = whoIsData;
  let dataBlocksFound = text.match(/(^[a-z]*:)([\w\W])*?((^source:)|(^(\w)*?ref:)).*(?=\n\n)/igm);
  if (dataBlocksFound !== null)
  {
    let firstIPRangeFoundPreviously = false;
    // iterate over each block
    dataBlocksFound.forEach( (dataBlock) =>
    {
      let dataFound = dataBlock.match(/(^[^%\s]\S*?:)(.*?$)/igm);
      if (dataFound !== null)
      {
        let tempBlock = {};
        dataFound.forEach( (data) =>
        {
          const pair    = data.split(':');
          let key       = pair[0].trim().toLowerCase();
          let value     = pair[1].trim();
          // grab every value
          if (tempBlock[key]) { tempBlock[key].push(value); }
          else                { tempBlock[key] = [value]; }
        });

        if ( (tempBlock['netrange'] || tempBlock['inetnum']) )
        {
          if (firstIPRangeFoundPreviously)
          {
            parsedBlocks.push(parsedBlockData);
            parsedBlockData = new ParsedBlock();
          }
          firstIPRangeFoundPreviously = true;
        }

        if (tempBlock['netrange']) { parsedBlockData.ip.range = tempBlock['netrange'][0]; }
        else if (tempBlock['inetnum']) { parsedBlockData.ip.range = tempBlock['inetnum'][0]; }
        if (tempBlock['cidr'])
        {
          parsedBlockData.ip.CIDR = tempBlock['cidr'][0];
        }
        if (tempBlock['netname'])
        {
          parsedBlockData.ip.network = tempBlock['netname'][0];
        }
        if (tempBlock['orgname'] || tempBlock['org-name'] || tempBlock['organisation'] || tempBlock['organization'])
        {
          if (!parsedBlockData.organization.name)
          {
            if(tempBlock['orgname']) { parsedBlockData.organization.name = tempBlock['orgname'][0]; }
            else if(tempBlock['org-name']) { parsedBlockData.organization.name = tempBlock['org-name'][0]; }
            else if(tempBlock['organisation']) { parsedBlockData.organization.name = tempBlock['organisation'][0]; }
            else if(tempBlock['organization']) { parsedBlockData.organization.name = tempBlock['organization'][0]; }
          }
        }
        if (tempBlock['org'] || tempBlock['orgid'])
        {
          if (!parsedBlockData.organization.id)
          {
            if(tempBlock['orgid']) { parsedBlockData.organization.id = tempBlock['orgid'][0]; }
            else if(tempBlock['org']) { parsedBlockData.organization.id = tempBlock['org'][0]; }
          }
        }
        if (tempBlock['descr'])
        {
          tempBlock['descr'].forEach( (descr) =>
          {
            parsedBlockData.organization.description += descr + '\n';
          });
          parsedBlockData.organization.description += '\n';
        }
        if (tempBlock['address'])
        {
          tempBlock['address'].forEach( (addr) =>
          {
            parsedBlockData.location.address += addr + '\n';
          });
          parsedBlockData.location.address += '\n';
        }
        if (tempBlock['city'] && !parsedBlockData.location.city)
        {
          parsedBlockData.location.city = tempBlock['city'][0];
        }
        if (tempBlock['stateprov'] && !parsedBlockData.location.state)
        {
          parsedBlockData.location.state = tempBlock['stateprov'][0];
        }
        if (tempBlock['postalcode'] && !parsedBlockData.location.postalcode)
        {
          parsedBlockData.location.postalcode = tempBlock['postalcode'][0];
        }
        if (tempBlock['country'] && !parsedBlockData.location.country)
        {
          parsedBlockData.location.country = tempBlock['country'][0];
        }
        if (tempBlock['refer'] && !parsedBlockData.source.refer)
        {
          parsedBlockData.source.refer = tempBlock['refer'][0];
        }
        if (tempBlock['source'] && !parsedBlockData.source.name)
        {
          parsedBlockData.source.name = tempBlock['source'][0];
        }
        if (tempBlock['status'] && !parsedBlockData.source.status)
        {
          parsedBlockData.source.status = tempBlock['status'][0];
        }
      }
    });
    parsedBlocks.push(parsedBlockData);
  }
  let returnIndex = 0;
  if (parsedBlocks.length>1)
  {
      // find the parsed block with the most specific IP range
      // lowest end IP
      let ipRanges = [];
      let lastLowestEnd = '255.255.255.255';
      let lastLowestIndex = parsedBlocks.length -1;
      let lastHighestEnd = '0.0.0.0';
      let lastHighestIndex = 0;
      for (index=0; index<parsedBlocks.length;index++)
      {
        let ranges = parsedBlocks[index].ip.range.split('-');
        ipRanges.push({'start': ipToInt(ranges[0].trim()), 'end': ipToInt(ranges[1].trim()), 'index': index});
        if (parsedBlocks[index].source.refer=='' && ipRanges[index].end < lastLowestEnd)
        {
          lastLowestEnd = ipRanges[index].end;
          lastLowestIndex = index;
        }
        if (parsedBlocks[index].source.refer=='' && ipRanges[index].end > lastHighestEnd)
        {
          lastHighestEnd = ipRanges[index].end;
          lastHighestIndex = index;
        }
      }
      if (detailedScrape)
        returnIndex = lastLowestIndex;
      else
        returnIndex = lastHighestIndex;
      // if we suddenly arrive at a whois db registrant we should send back an empty reply
      // APNIC
      // ARIN
      // RIPE
      // LACNIC
      // AFRINIC
      if (    ipRanges[returnIndex].start < ipToInt(parsedIPaddress)
          &&(     parsedBlocks[returnIndex].ip.network == 'APNIC-AP'
              // ||
            )
        )
      {
        parsedBlocks[returnIndex] = new ParsedBlock();
      }
  }

  delete parsedBlocks[returnIndex].source;

  return parsedBlocks[returnIndex];
};

/**
 * 
 * @param {string} currentIP 
 * @returns {string} nextIP - the next ip to lookup
 */
const getNextIP = (lastIP, level) =>
{
  let nextIP = null;
  for (var x = level; x>=0; x--)
  {
    let next = Number(lastIP[x])+1;
    if (next<256)
    {
      lastIP[x] = next;
      // smooth out ips to floor
      if (x==level && level<3) { for (z=(level+1);z<4;z++) { lastIP[z] = '0'; } }
      // LOG iteration at top level
      // if (x===0) { log(lastIP[0]+'.'+lastIP[1]+'.'+lastIP[2]+'.'+lastIP[3], 'getNextIP', currentIP, 'info', true); }
      break;
    }
    else
    {
      if (x===0)
      {
        return null;
      }
      else
      {
        // smooth out ips to floor
        for (z=x+1;z<4;z++) { lastIP[z] = '0'; }
        lastIP[x] = 0;
      }
    }
  }
  nextIP = lastIP[0]+'.'+lastIP[1]+'.'+lastIP[2]+'.'+lastIP[3]
  return nextIP;
};

/**
 * the meat of this script
 */
const ipScraper = async () =>
{
  let firstIP = '0.0.0.0';
  let organization = '';
  let location = '';
  let level = 3;

  const args = process.argv.slice(2);
  if (args.length)
  {
    // HELP
    if (args.includes('help')||args.includes('-h')||args.includes('--help'))
    {
      helpFeedback();
      handleProgramTermination('suppress');
    }
    else
    {
      // SPECIFY LOWEST LEVEL OF IP TO INCREMENT WHEN GETTING THE NEXT IP TO SCRAPE
      if (args.includes('-1'))
      {
        level = 0;
      }
      else if (args.includes('-2'))
      {
        level = 1;
      }
      else if (args.includes('-3'))
      {
        level = 2;
      }
      // SPECIFY IP ADDRESS AT WHICH SCRAPING STARTS
      if (args.includes('ipaddress')||args.includes('-i')||args.includes('--ipaddress'))
      {
        let index = args.indexOf('--ipaddress');
        if (index == -1)
        {
          index = args.indexOf('-i');
          if (index == -1)
            index = args.indexOf('ipaddress');
        }
        if ( args[index + 1] && args[index + 1].split('.').length === 4 )
        {
          firstIP = args[index + 1];
        }
        else
        {
          log('No IP address specified', 'ipScraper', args, 'err', true);
          helpFeedback();
          handleProgramTermination('suppress');
        }
      }
      // SPECIFY STRING TO SEARCH FOR RELATED TO ORGANIZATION
      if (args.includes('organization')||args.includes('-o')||args.includes('--organization'))
      {
        let index = args.indexOf('--organization');
        if (index == -1)
        {
          index = args.indexOf('-o');
          if (index == -1)
            index = args.indexOf('organization');
        }

        if (args[index+1] && !COMMANDS.includes(args[index+1]))
        {
          organization = args[index+1].toLowerCase();
        }
        else
        {
          log('No organization specified', 'ipScraper', args, 'err', true);
          helpFeedback();
          handleProgramTermination('suppress');
        }
        
      }
      // SPECIFY STRING TO SEARCH FOR RELATED TO LOCATION
      if (args.includes('location')||args.includes('-l')||args.includes('--location'))
      {
        let index = args.indexOf('--location');
        if (index == -1)
        {
          index = args.indexOf('-l');
          if (index == -1)
            index = args.indexOf('location');
        }

        if (args[index+1] && !COMMANDS.includes(args[index+1]))
        {
          location = args[index+1].toLowerCase();
        }
        else
        {
          log('No location specified', 'ipScraper', args, 'err', true);
          helpFeedback();
          handleProgramTermination('suppress');
        }
      }
    }
  }

  thisIP = firstIP;
  process.stdout.write('[\n');
  let subsequent_pass = false;
  while (thisIP != null)
  {
    let result = null;
    try
    {
      // receive the result of a lookup of thisIP
      result = await lookup(thisIP);
  
      // prepare the output - if we are to display it 
      let stdout_result = '';
      if (subsequent_pass)  { stdout_result = stdout_result + (',\n').toString(); }
      else                  { subsequent_pass = true; }
          stdout_result += JSON.stringify(result,null,2);

      // no queries set so we 'record' every result
      if(organization==='' && location==='')
      {
        process.stdout.write(stdout_result);
      }
      // a string to look for has been specified
      // so we only 'record' results which match the query
      else
      {
        let organization_result_to_search = '';
        if ( organization.length ) { organization_result_to_search = result.organization.name + ' . ' + result.organization.id + ' . ' + result.organization.description; }
        let location_result_to_search = '';
        if ( location.length ) { location_result_to_search = result.location.address + ' . ' + result.location.city + ' . ' + result.location.state + ' . ' + result.location.postalcode + ' . ' + result.location.country; }

        if  ( organization_result_to_search.length )
        {
          // looking for both organization and location
          if (location_result_to_search.length)
          {
            if (location_result_to_search.toLowerCase().includes(location) && organization_result_to_search.toLowerCase().includes(organization))
            {
              process.stdout.write(stdout_result);
            }
          }
          // only looking for organization
          else if (organization_result_to_search.toLowerCase().includes(organization))
          {
            process.stdout.write(stdout_result);
          }
        }
        // only looking for location
        else if (location_result_to_search.toLowerCase().includes(location))
        {
          process.stdout.write(stdout_result);
        }
      }
    }
    catch (err) { }

    // failsafe for dealing with bad data
    let checkFromThisIP = ['0','0','0','0'];
    if (!result.ip.range.includes('-'))
    {
      checkFromThisIP         = thisIP.split('.');
      checkFromThisIP[level]  = '255';
    }
    else
    {
      let IPGroups = result.ip.range.split('-');
      checkFromThisIP = IPGroups[1].trim().split('.');
    }

    thisIP = getNextIP(checkFromThisIP, level);
  }
  handleProgramTermination();
};

module.exports = ipScraper();
