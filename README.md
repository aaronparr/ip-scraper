# Whois Scraper

This command line tool scrapes IP address metadata using recursive calls of the `whois` command on *nix-like systems. The results are output as JSON records to stdio. So you can look at them in the terminal or pipe them to other tools or files.

When you execute the tool, it queries IP addresses in sequential order beginning with the IP address you provide, and continuing with the next IP address past the range of each subsequent record it finds. If you provide no address, it will begin querying the whois db for `0.0.0.0`.

**WARNING:** If you use this tool often, you will exceed your daily allotted requests to the whois dbs.

## Installation

0. [Install Node.js](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
1. Clone the repository.
2. `$ cd ip-scraper` - enter the repo
3. `$ sudo chmod +x ./index.js` - Make index.js executable
4. Add index.js to your path with the alias `scrape` or something similar.
    * Make a symlink using a command similar to the following: `$ ln -s ~/Code/ip-scraper/index.js ~/Code/.bin/scrape`. **NOTE:** `~/Code/.bin/` is on my path.

## Use

After completing the above:
* `$ scrape -h` - to see the list of options
* `$ scrape -i 65.0.0.0` - to begin scraping metadata at IP 65.0.0.0 and output the data to the console.
* `$ scrape -o amazon` - to begin scraping at 0.0.0.0, only outputting records with organizations that have `amazon` in the name.
* `$ scrape -o google > google_ips.json` - look for records for google and output the results to the file `google_ips.json`.